# MQTT Client

> Script to generate MQTT messages.

## Getting Started

Here's what you need to do.

### Prerequisites

Install npm packages.

```
npm install
```

Fill in required application credentials and root UUID of the identity network to script.js

```
const url = '<broker-url>'
const username = '<username>'
const password = '<password>'
```

## Running the crawler

Client starts with the following command.

```
npm start
```
