"use strict";
/**
 * Module dependencies.
 */
const mqtt = require('mqtt')

const url = '<broker-url>'
const username = '<username>'
const password = '<password>'

const options = {
    username,
    password
};

const genRand = function (min, max, decimalPlaces) {
    const rand = Math.random() * (max - min) + min;
    const power = Math.pow(10, decimalPlaces);
    return Math.floor(rand * power) / power;
};

const ts = new Date(Date.now()).toISOString();

const order = {
    "type": "Process",
    "name": "Order",
    "timestamp": "<DateTime>",
    "instructionTime": "<POS_Timestamp>",
    "startDateTime": "<Start_timestamp>",
    "executor": {
        "type": "Order",
        "idLocal": "<Tilausnumero>"
    },
    "process": [
        {
            "type": "Process",
            "name": "ChildOrder",
            "timestamp": "<DateTime>",
            "executor": {
                "type": "Order",
                "idLocal": "<Tilausnumero>"
            },
            "orderId": "<Tilausnumero>",
            "reelCount": "<Integer>",
            "reel": [
                {
                    "type": "Reel",
                    "width": "<Width>",
                    "length": "<Length>",
                    "diameter": "<Diameter>",
                    "density": "<Density>",
                    "amountWrapping": "<Wrapping>",
                    "code": "<MaterialNumber>",
                    "ReelPackingTime": "<PACKINF_timestamp>",
                    "ReelLabelingTime": "<PPO_Timestamp>"
                }
            ],
            "pallet": [
                {
                    "type": "Pallet",
                    "categorizationLocal": "<Pallet_type>",
                    "itemsPerLayer": "<Rolls_per_layer>",
                    "layerAmount": "<Layers_per_pallet>",
                    "itemsTotal": "<Rolls_per_pallet>",
                    "PalletTop": "<Pallet_top>",
                    "PalletIntermediate": "<Pallet_intermediate>"
                }
            ]
        }
    ]
}

const slitting = {
    "type": "Process",
    "name": "Slitting",
    "timestamp": "<DateTime>",
    "executor": {
        "type": "Machine",
        "idLocal": "<WorkcenterId>",
        "categorizationLocal": "Slitter"
    },
    "instructionTime": "<POS_Timestamp>",
    "startDateTime": "<Start_timestamp>",
    "operatorId": "4",
    "orderId": "<Tilausnumero>",
    "reelId": "<ReelId>"
};

const moving = {
    "type": "Process",
    "name": "Moving",
    "timestamp": "<DateTime>",
    "executor": {
        "type": "Machine",
        "idLocal": "<RobotId>",
        "categorizationLocal": "Robot"
    },
    "status": "<Status>",
    "error": 0,
    "errorMessage": "",
    "reelId": "<ReelId>",
    "process": [
        {
            "type": "Process",
            "name": "Moving",
            "timestamp": "<DateTime>",
            "executor": {
                "type": "Machine",
                "idLocal": "Tulokuljetin_1",
                "categorizationLocal": "Feeder"
            },
            "robotId": "<RobotId>",
            "status": "<Status>"
        },
        {
            "type": "Process",
            "name": "Moving",
            "timestamp": "<DateTime>",
            "executor": {
                "type": "Machine",
                "idLocal": "Tulokuljetin_2",
                "categorizationLocal": "Feeder"
            },
            "robotId": "<RobotId>",
            "status": "<Status>"
        }
    ]
};

const wrapping = {
    "type": "Process",
    "name": "Wrapping",
    "timestamp": "<DateTime>",
    "executor": {
        "type": "Machine",
        "idLocal": "<WrapperId>",
        "categorizationLocal": "Wrapper"
    },
    "status": "<Status>",
    "process": [
        {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Paikka_1",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>"
        }, {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Paikka_2",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>"
        }, {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Paikka_3",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>"
        }, {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Paikka_4",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>"
        }, {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Paikka_5",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>"
        }, {
            "type": "Process",
            "name": "Moving",
            "executor": {
                "type": "Machine",
                "idLocal": "Output_1",
                "categorizationLocal": "Conveyor"
            },
            "error": 0,
            "wrapperId": "<WrapperId>",
            "reelId": "<ReelId>",
            "completed": "<DateTime>"
        }
    ]
}

const qualityCheck = {
    "type": "Process",
    "name": "QualityCheck",
    "timestamp": "<DateTime>",
    "executor": {
        "type": "Order",
        "idLocal": "<OrderId>"
    },
    "reelId": "<ReelId>",
    "width": "<Width>",
    "length": "<Length>",
    "diameter": "<Diameter>",
    "completed": "<Timestamp>",
    "quality": "<Quality>",
    "qualityReason": "<ReasonCode>"
}

let interval = 5000;
let id = genRand(192376, 192380, 0);
let orderId = 'Tilaus_' + id;
let reelId = id + genRand(100000000, 100000100, 0);

const client = mqtt.connect(url, options);

const generateOrder = function (template, parent, index) {
    let order = {...template};
    /** Order. */
    order.timestamp = new Date(Date.now()).toISOString();
    order.orderId = 'Tilaus_' + parent;
    order.executor.idLocal = 'Tilaus_' + (parent + index);
    order.reelCount = genRand(1, 5, 0);
    order.palletCount = genRand(1, 2, 0);

    // Store orderId.
    id = parent;
    orderId = order.executor.idLocal;

    return order;
}

const generateReel = function (template) {
    let reel = {...template};
    /** Reel. */
    reel.width = 100 * genRand(1, 5, 0);
    reel.length = 100 * genRand(1, 5, 0);
    reel.diameter = 20 * genRand(1, 5, 0);
    reel.density = genRand(0.1, 0.5, 1);
    reel.amountWrapping = 10 * genRand(1, 5, 0);
    reel.code = 1 * genRand(1, 5, 0);
    reel.ReelPackingTime = new Date(Date.now() - 1000 * 40).toISOString();
    reel.ReelLabelingTime = new Date(Date.now() - 1000 * 20).toISOString();
    return reel;
}

const generatePallet = function (template) {
    let pallet = {...template};
    /** Pallet. */
    pallet.categorizationLocal = 'Pallet';
    pallet.itemsPerLayer = 100 * genRand(1, 5, 0);
    pallet.layerAmount = 2;
    pallet.itemsTotal = pallet.itemsPerLayer * pallet.layerAmount;
    pallet.PalletTop = pallet.itemsPerLayer;
    pallet.PalletIntermediate = pallet.itemsPerLayer;
    return pallet;
}

client.on('connect', function () {
    client.subscribe('presence', function (err) {
        if (!err) {
            setInterval(() => {
                const message = {...order, timestamp: new Date(Date.now()).toISOString()};
                const id = genRand(192376, 192380, 0);
                message.executor.idLocal = 'Tilaus_' + genRand(192376, 192380, 0);
                message.instructionTime = new Date(Date.now() - 1000 * 30).toISOString();
                message.startDateTime = new Date(Date.now() - 1000 * 15).toISOString();
                message.orderCount = genRand(1, 5, 0);
                const orderTemplate = Array.isArray(message.process) ? {...message.process[0]} : null;
                delete message.process;

                // Send main order.
                // <--
                // console.log(message);
                client.publish('processes/' + message['name'], JSON.stringify(message));
                // -->

                if (!orderTemplate) return;
                for (let i = 0; i < message.orderCount; i++) {
                    const child = generateOrder(orderTemplate, id, i + 1);
                    const reelTemplate = child.reel[0];
                    child.reel = [];
                    for (let j = 0; j < child.reelCount; j++) {
                        child.reel.push(generateReel(reelTemplate));
                    }
                    const palletTemplate = child.pallet[0];
                    child.pallet = [];
                    for (let k = 0; k < child.palletCount; k++) {
                        child.pallet.push(generatePallet(palletTemplate));
                    }

                    // Send sub order.
                    // <--
                    // console.log(child);
                    client.publish('processes/' + child['name'], JSON.stringify(child));
                    // -->
                }
            }, interval * 10)
            setInterval(() => {
                const message = {...slitting, timestamp: new Date(Date.now()).toISOString()};
                message.executor.idLocal = "Workcenter-0" + genRand(1, 5, 0);
                message.orderId = 'Tilaus_' + id;
                message.operatorId = '' + genRand(1, 5, 0) + '';
                reelId = id + genRand(100000000, 100000100, 0);
                message.reelId = reelId;
                message.instructionTime = new Date(Date.now() - 1000 * 30).toISOString();
                message.startDateTime = new Date(Date.now() - 1000 * 15).toISOString();

                // Send slitting.
                // <--
                // console.log(message);
                client.publish('processes/' + message['name'], JSON.stringify(message));
                // -->
            }, interval * 2)
            setInterval(() => {
                const message = {...moving, timestamp: new Date(Date.now()).toISOString()};
                message.executor.idLocal = "Robot-" + genRand(40, 43, 0);
                message.reelId = reelId;
                message.status = genRand(0, 5, 0);
                message.error = 0;
                message.errorMessage = '';
                // 5% Change of error.
                if (Math.random() > 0.95) {
                    message.error = 1;
                    message.errorMessage = 'Casual error.';
                    if (Math.random() > 0.5) {
                        message.error = 2;
                        message.errorMessage = 'Systematic error.';
                    }
                }

                const children = message.process;
                delete message.process;

                // Send Moving.
                // <--
                // console.log(message);
                client.publish('processes/' + message['name'], JSON.stringify(message));
                // -->

                for (let i = 0; i < children.length; i++) {
                    const child = children[i];
                    child.timestamp = new Date(Date.now()).toISOString();
                    child.robotId = message.executor.idLocal;
                    child.status = genRand(0, 5, 0);

                    // Send sub moving.
                    // <--
                    // console.log(child);
                    client.publish('processes/' + child['name'], JSON.stringify(child));
                    // -->
                }
            }, interval)
            setInterval(() => {
                const message = {...wrapping, timestamp: new Date(Date.now()).toISOString()};
                message.executor.idLocal = "Wrapper-0" + genRand(1, 5, 0);
                message.status = genRand(0, 5, 0);
                const children = message.process;
                delete message.process;

                // Send Wrapping.
                // <--
                // console.log(message);
                client.publish('processes/' + message['name'], JSON.stringify(message));
                // -->

                for (let i = 0; i < children.length; i++) {
                    const child = children[i];
                    child.timestamp = new Date(Date.now()).toISOString();
                    child.wrapperId = message.executor.idLocal;
                    child.reelId = reelId;
                    if (child.completed) {
                        child.completed = new Date(Date.now() - 1000 * 20).toISOString();
                    }

                    // Send sub moving.
                    // <--
                    // console.log(child);
                    client.publish('processes/' + child['name'], JSON.stringify(child));
                    // -->
                }
            }, interval)
            setInterval(() => {
                const message = {...qualityCheck, timestamp: new Date(Date.now()).toISOString()};
                message.executor.idLocal = orderId;
                message.reelId = reelId;
                message.width = 100 * genRand(1, 5, 0);
                message.length = 100 * genRand(1, 5, 0);
                message.diameter = 20 * genRand(1, 5, 0);
                message.completed = new Date(Date.now() - 1000 * 30).toISOString();
                message.quality = 1;
                // 10% Change of bad quality.
                if (Math.random() > 0.90) message.quality = 0;
                message.qualityReason = message.quality === 0 ? genRand(1, 5, 0) : 0;

                // Send quality check.
                // <--
                // console.log(message);
                client.publish('processes/' + message['name'], JSON.stringify(message));
                // -->

            }, interval)
            setInterval(() => {
                interval = genRand(1000, 10000, 0);
            }, 2500)
        }
    })
})

client.on('message', function (topic, message) {
    // message is Buffer.
    console.log(message.toString());
})
